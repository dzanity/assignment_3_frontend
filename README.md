# Assignment-3 (Frontend)

## Link gitlab backend: https://gitlab.com/dzanity/assignment_3
## Link gitlab frontend: https://gitlab.com/dzanity/assignment_3_frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm build
```